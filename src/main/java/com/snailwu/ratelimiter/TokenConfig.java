package com.snailwu.ratelimiter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;

import javax.annotation.Resource;

/**
 * @author WuQinglong
 */
@Configuration
public class TokenConfig {

    @Resource
    private RedisConnectionFactory redisConnectionFactory;

    /**
     * 示例接口限流
     */
    @Bean(name = "demoRateLimiter")
    public RedisRateLimiter demoRateLimiter() {
        return new RedisRateLimiter("token:api:demo", 4, redisConnectionFactory);
    }

}
